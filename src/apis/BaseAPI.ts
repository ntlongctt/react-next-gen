interface ObjectParam {
  [key: string]: number | boolean | string
}

interface RequestPayload {
  params: any;
  body: any;
  headers: any;
}

const RespType = {
 Unauthorized: 'Unauthorized',

}

type RespType = 'Unauthorized' | 'Success' | 'ServerError';

const handleUnauthorized = () => 're login';
const handleSuccess = () => 'success!!!';
const handleServerError = () => 'ServerError T_T';

const RespHandler = {
  ''
}

const getRespStatus = (code: number): string => {
  return code === 401 ? Respty 200 <= code && code <= 300) ? 'Success' : 'ServerError'
}

const getToken = (): string => 'token';

const paramToQueryString = (params: ObjectParam): string =>
  Object.keys(params).map((key: string) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`).join('&');

const request = async (url: string, payload: RequestPayload, method = 'GET'): Promise<any> => {
  const {headers = {}, params = {}, body = {}} = payload || {};
  const option = {
    method,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      'Authorization': getToken(),
      ...headers
    },
    body: JSON.stringify(body)
  }
  
  const response$ = await fetch(url, option);

  const resp = await response$.json();
  return resp;

}
