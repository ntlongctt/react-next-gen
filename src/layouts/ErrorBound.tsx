import type { BreadcrumbProps } from '@primer/components';
import React from 'react';

interface ErrorBoundState {
  hasError: boolean
}

interface ErrorBoundProps {
  children: React.ReactElement
}

class ErrorBound extends React.Component<BreadcrumbProps, ErrorBoundState> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false } as ErrorBoundState;
  }

  static getDerivedStateFromError() {
    return { hasError: true }
  }

  componentDidCatch(error: Error | null, errorInfo: React.ErrorInfo) {
    console.log(errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong!!!</h1>
    }
    return this.props.children;
  }
}

export default ErrorBound;
