import Recoil from 'recoil';

export const atom = Recoil.atom;
export const RecoilRoot = Recoil.RecoilRoot;