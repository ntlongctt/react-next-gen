import React from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './pages/home';

import { atom } from './recoilWrap';
import { ErrorBound } from './layouts';

export const list = atom({
    key: "list",
    default: []
});

export const filterListValue = atom({
    key: "filterListValue",
    default: "" //values: all or filter
});

interface AppProps { }

function App({ }: AppProps) {
  return (
    <ErrorBound>
      <Home />
    </ErrorBound>
  );
}

export default App;
