import React from 'react'
import Counter from '../Counter';

const Home: React.FC = () => {
  const a = [undefined].map(i => console.log(i.toString()));
  return (
    <div>
      Home page
      <Counter />
    </div>
  )
}

export default Home;