import React from 'react';

const Counter: React.FC = () => {
  return (
    <div>
      <h1>Counter</h1>
      <span>Total:</span>
      <button>Increase counter</button>
    </div>
  )
}

export default Counter;
